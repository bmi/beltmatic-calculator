﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Runtime.Remoting.Messaging;
using System.Threading;

namespace RozkladCisla
{
    internal class BCalc
    {
        // modify allower numbers here
        static int[] numbers = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 11, 12, 13,
            14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31 };

        static int maxResult = 10000000;

        static List<string> results = new List<string>();
        static List<Tuple<string, int>> data = new List<Tuple<string, int>>();

        static public void Start()
        {
            while (true)
            {

                Console.Clear();

                var strMaxExtractionNumber = ConfigurationManager.AppSettings["maxExtractionNumber"];
                var maxExtractionNumber = int.Parse(strMaxExtractionNumber);


                numbers = Enumerable.Range(1, maxExtractionNumber).Where(i => i != 10).ToArray();

                var strMaxResult = ConfigurationManager.AppSettings["maxResult"];
                maxResult = int.Parse(strMaxResult);

                data.Clear();
                results.Clear();
                createExps();
                createMuls();


                Console.WriteLine("BeltmaticCalculator");
                Console.WriteLine(string.Join(", ", numbers));
                Console.WriteLine($"maxResult: {maxResult}");
                Console.WriteLine($"last: {ConfigurationManager.AppSettings["last"]}");




                Console.Write("Enter number: ");
                var str = Console.ReadLine();
                int num = int.Parse(str);
                Console.WriteLine();
                try
                {
                    new BCalc(num);
                    var cf = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
                    cf.AppSettings.Settings["last"].Value = num.ToString();
                    cf.Save(ConfigurationSaveMode.Modified);
                    ConfigurationManager.RefreshSection("appSettings");
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.Message);
                }
                Console.WriteLine("Key (x for exit)");
                var c = Console.ReadKey();
                if (c.KeyChar == 'x')
                    break;
            }

        }


        public BCalc(int num)
        {

            int numBkp = num;

            while (num != 0)
            {
                num = search(num);
            }

            Console.WriteLine(numBkp);


            var exp = string.Join(" + ", results);
            int ires = EvaluateDT(exp);

            Console.WriteLine($"{exp} = {ires}");

        }

        private int search(int rest)
        {


            if (rest == 0)
                return 0;

            if (numbers.Contains(rest))
            {
                results.Add(rest.ToString());
                rest = 0;
                return 0;
            }

            // find exact result
            var ff = data.FirstOrDefault(i => i.Item2 == rest);
            if (ff != null)
            {
                results.Add(ff.Item1);
                return 0;
            }


            int bestDiff = int.MaxValue;
            Tuple<string, int> best = null;

            foreach (var item in data)
            {
                var diff = rest - item.Item2;
                if (diff < 0) continue;
                if (diff < bestDiff)
                {
                    bestDiff = diff;
                    best = item;
                }
            }

            if (best != null)
            {
                results.Add(best.Item1);
                rest -= best.Item2;
            }
            else
            {
                Console.WriteLine("Not found");
            }

            return rest;
        }

        static private void createMuls()
        {
            foreach (var i in numbers)
                foreach (var j in numbers)
                    data.Add(Tuple.Create($"{i} * {j}", i * j));
        }

        static private void createExps()
        {
            foreach (var @base in numbers.Skip(1))
            {
                int exp = 2;
                while (true)
                {
                    int val = (int)Math.Pow(@base, exp);
                    if (val < maxResult)
                    {
                        if (exp == 2)
                            data.Add(Tuple.Create($"{@base} * {@base}", val));
                        else
                            data.Add(Tuple.Create($"{@base} ^ {exp}", val));
                        exp++;
                        if (exp == 10)
                            exp++;
                    }
                    else
                    {
                        break;
                    }
                }
            }

        }

        public static int EvaluateDT(string expression)
        {

            while (expression.Contains("^"))
            {
                var idx = expression.IndexOf("^");
                expression = pow2mul(expression, idx);
            }


            var dataTable = new DataTable();
            dataTable.Columns.Add("expression", typeof(int), expression);
            DataRow row = dataTable.NewRow();
            dataTable.Rows.Add(row);
            return (int)row["expression"];
        }

        private static string pow2mul(string str, int idx)
        {
            var left = str.Substring(0, idx - 1);
            var right = str.Substring(idx + 2);

            var lparts = left.Split(' ');
            string slnum = lparts.Last();
            int lnum = int.Parse(slnum);

            var rparts = right.Split(' ');
            string srnum = rparts[0];
            int rnum = int.Parse(srnum);

            ulong pow = (ulong)Math.Pow(lnum, rnum);

            string newstr = str;

            int delfrom = idx - 1 - slnum.Length;
            int delto = idx + 1 + srnum.Length;

            newstr = newstr.Remove(delfrom, delto - delfrom + 1);
            newstr = newstr.Insert(delfrom, pow.ToString());

            return newstr;
        }
    }
}